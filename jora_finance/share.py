from yahoo_finance import Share as YahooShare
import csv
import urllib2
from datetime import datetime, timedelta
import numpy as np
import os

class Share(object):

    def __init__(self, symbol):
	self.__symbol = symbol           # Save the symbol, e.g. NAM.AX
        try:
            print("creating yahoo object...")
	    self.__YS = YahooShare(symbol)   # Create the yahoo_finance Share class instance
            print("yahoo object created!")
            self.__online = True
        except:
            self.__online = False
            print("Could not establish connection to Yahoo Finance. Running in offline mode")
            pass
        

    def get_price(self):
        return self.__YS.data_set['LastTradePriceOnly']

    def time_series(self, start, end):
        #This should fetch from API or locally, depending on if it was cached

        # convert the date strings into actual date objects
        start_dt = datetime.strptime(start, "%Y-%m-%d")
        end_dt   = datetime.strptime(end  , "%Y-%m-%d")

        # fetch any data that has been cached locally
        data_dir = (os.path.expanduser("~") + os.sep + \
                    ".local" + os.sep + \
                    "share" + os.sep + \
                    "pyslap" + os.sep + \
                    "sharecache" + os.sep)
        # create the path
        if not os.path.isdir(data_dir):
            os.makedirs(data_dir)


        # check if the file exists
        filename = self.__symbol + ".cache.npy"
        if os.access(data_dir + filename, os.R_OK):
            data = np.load(data_dir + filename)
            # now check the range of values. do we have to fetch more for the cache?
            cachedStartDt = data[-1][0].astype(datetime) # the first column, last row is start date
            fetchStartDt = cachedStartDt - timedelta(days=1)
            cachedEndDt = data[0][0].astype(datetime) # the first column, first row is end date
            fetchEndDt = cachedEndDt + timedelta(days=1)
            
            # see if there's anything to prepend
            if start_dt.date() < fetchStartDt:
                print("start_dt=" + str(start_dt.date()) + "; fetchStartDt=" + str(fetchStartDt) + ", will fetch more")
                data_to_prepend = self.__getYahooCsv(start_dt, fetchStartDt)
                if data_to_prepend.size == 0:
                    print("There was no data to fetch and prepend")
                else:
                    print(data)
                    print(data_to_prepend)
                    data = np.vstack((data, data_to_prepend))
                    print(data)
            else:
                print("start_dt=" + str(start_dt.date()) + "; fetchStartDt=" + str(fetchStartDt) + ", nothing to fetch")

            # see if there's anything to append
            if end_dt.date() > fetchEndDt:
                print("end_dt=" + str(end_dt.date()) + "; fetchEndDt=" + str(fetchEndDt) + ", will fetch more")
                data_to_append = self.__getYahooCsv(fetchEndDt, end_dt)
                print(data_to_append)
                if data_to_append.size == 0:
                    print("There was no data to fetch and append")
                else: 
                    print(data)
                    print(data_to_append)
                    data = np.vstack((data_to_append, data))
                    print(data)
            else:
                print("end_dt=" + str(end_dt.date()) + "; fetchEndDt=" + str(fetchEndDt) + ", nothing to fetch")

        else:
            # have to grab everything
            data = self.__getYahooCsv(start_dt, end_dt)

        # now save it to file
        np.save(filename + self.__symbol + ".cache", data)

	#data = np.load(data_dir + filename + ".cache.npy")
	#print(data)

        #cr = csv.reader(response)
        #for row in cr:
        #    print row

    def __getYahooCsv(self, start_dt, end_dt):
        # construct the url
        url = 'http://real-chart.finance.yahoo.com/table.csv?s=' +  \
               self.__symbol + \
               '&d=' + str(end_dt.month - 1) + \
               '&e=' + str(end_dt.day) + \
               '&f=' + str(end_dt.year) + \
               '&g=d' + \
               '&a=' + str(start_dt.month - 1) + \
               '&b=' + str(start_dt.day) + \
               '&c=' + str(start_dt.year) + \
               '&ignore=.csv'

	
        print url
        response = urllib2.urlopen(url)

        #print(response)

        try:
            data = np.genfromtxt(response.read(),\
                                 dtype=[('mydate','datetime64[D]'),\
                                        ('open','d8'),\
                                        ('high','d8'),\
                                        ('low','d8'),\
                                        ('close','d8'),\
                                        ('volume','i8'),\
                                        ('adj','d8')],\
                                 delimiter=',',\
                                 skip_header=1)
        except:
            print("csv parse failed")
            data = None
            pass

        return data

	

